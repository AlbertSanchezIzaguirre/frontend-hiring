import React from 'react';
import ReactDOM from 'react-dom';

import { LogicProvider } from './logic';
import Theme from './Theme';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <Theme>
      <LogicProvider>
        <App />
      </LogicProvider>
    </Theme>
  </React.StrictMode>,
  document.getElementById('root')
);
