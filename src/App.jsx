import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

import { withLogic } from './logic';

const style = {
  root: {
    display: 'grid',
    height: '100vh',
    gridTemplateRows: 'auto 1fr',
  },
  scroll: {
    position: 'relative',
    padding: '0px 16px 0px 16px',
    overflow: 'auto',
  },
  card: {
    margin: '16px 0px 16px 0px',
  },
  cardActions: {
    justifyContent: 'flex-end',
  },
  picker: {
    paddingTop: 16,
  },
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isValidForm: true,
    };
  }

  handleImage(event) {
    const file = event.target.files[0];
    if (file && (file.type === 'image/png' || file.type === 'image/jpeg')) {
      const image = URL.createObjectURL(file);
      this.props.Logic.updateForm({
        images: [
          ...this.props.Logic.form.images,
          image
        ],
      });
    }
  }

  submitForm(form) {
    let isValidForm = true;
    if (form.type === 'default') {
      isValidForm = false;
    }
    if (form.title === '' || form.description === '') {
      isValidForm = false;
    }
    if (form.type !== 'idea') {
      if (form.priority === '') {
        isValidForm = false;
      }
      if (new Date(form.deadline) < new Date(new Date().toJSON().slice(0, 10).replace(/-/g, '-'))) {
        isValidForm = false;
      }
    }
    if (isValidForm) {
      this.props.Logic.submitForm(form);
    }
    this.setState({ isValidForm: isValidForm });
  }

  render() {
    const { Logic } = this.props;
    const { isValidForm } = this.state;
    return (
      <div style={style.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6">
              Rap Battles Dojo
            </Typography>
          </Toolbar>
        </AppBar>
        <div style={style.scroll}>
          <Grid spacing={2} alignItems="center" justify="space-between" container>
            <Grid item />
            <Grid xs={12} sm={10} md={8} lg={6} item>
              <Card style={style.card}>
                <CardContent>
                  <form autoComplete="off">
                    <Grid spacing={2} container>
                      <Grid xs={12} sm={8} item>
                        <TextField
                          label="Title"
                          variant="outlined"
                          value={Logic.form.title}
                          error={!isValidForm && Logic.form.title === ''}
                          onChange={event => Logic.updateForm({ title: event.target.value })}
                          fullWidth
                          required
                        />
                      </Grid>
                      <Grid xs={12} sm={4} item>
                        <TextField
                          label="Type"
                          variant="outlined"
                          value={Logic.form.type}
                          SelectProps={{ native: true }}
                          error={!isValidForm && Logic.form.type === 'default'}
                          onChange={event => Logic.updateForm({ type: event.target.value })}
                          fullWidth
                          required
                          select
                        >
                          <option value="default" disabled>
                            Select
                          </option>
                          <option value="Feature Request">
                            Feature Request
                          </option>
                          <option value="Bug Fix">
                            Bug Fix
                          </option>
                          <option value="Idea">
                            Idea
                          </option>
                        </TextField>
                      </Grid>
                      { Logic.form.type !== 'Idea' && (
                        <Grid xs={12} sm={7} item>
                          <FormControl component="fieldset" error={!isValidForm && Logic.form.priority === ''}>
                            <FormLabel component="legend">
                              Priority 1 - 5, where 5 is urgent
                            </FormLabel>
                            <RadioGroup
                              value={Logic.form.priority}
                              onChange={event => Logic.updateForm({ priority: event.target.value })}
                              row
                            >
                              <FormControlLabel value="1" control={<Radio />} label="1" />
                              <FormControlLabel value="2" control={<Radio />} label="2" />
                              <FormControlLabel value="3" control={<Radio />} label="3" />
                              <FormControlLabel value="4" control={<Radio />} label="4" />
                              <FormControlLabel value="5" control={<Radio />} label="5" />
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                      ) }
                      { Logic.form.type !== 'Idea' && (
                        <Grid xs={12} sm={5} item>
                          <TextField
                            type="date"
                            label="Deadline"
                            variant="outlined"
                            value={Logic.form.deadline}
                            onChange={event => Logic.updateForm({ deadline: event.target.value })}
                            error={!isValidForm && new Date(Logic.form.deadline) < new Date(new Date().toJSON().slice(0, 10).replace(/-/g, '-'))}
                            fullWidth
                            required
                          />
                        </Grid>
                      ) }
                      <Grid xs={12} sm={12} item>
                        <TextField
                          variant="outlined"
                          label="Description"
                          value={Logic.form.description}
                          error={!isValidForm && Logic.form.description === ''}
                          onChange={event => Logic.updateForm({ description: event.target.value })}
                          fullWidth
                          multiline
                          required
                        />
                      </Grid>
                      { Logic.form.images.map(image => (
                        <Grid xs={4} key={image} item>
                          <div
                            style={{
                              display: 'block',
                              width: '100%',
                              paddingTop: '100%',
                              backgroundImage: `url(${image})`,
                              backgroundRepeat: 'no-repeat',
                              backgroundPosition: 'center',
                              backgroundSize: 'cover',
                            }}
                          />
                        </Grid>
                      )) }
                      <Grid xs={12} item>
                        <div style={style.picker}>
                          <Button variant="outlined" component="label">
                            Select Image
                            <input type="file" accept="image/png; image/jpeg;" onChange={event => this.handleImage(event)} hidden />
                          </Button>
                        </div>
                      </Grid>
                    </Grid>
                  </form>
                </CardContent>
                <CardActions style={style.cardActions}>
                  <Button variant="outlined" onClick={() => { this.setState({ isValidForm: true }); Logic.resetForm(); }}>
                    Reset
                  </Button>
                  <Button color="primary" variant="contained" onClick={() => this.submitForm(Logic.form)}>
                    Submit
                  </Button>
                </CardActions>
              </Card>
              { Logic.pastForms.length > 0 && Logic.pastForms.map(pastForm => (
                <Card style={style.card} key={pastForm.date}>
                  <CardContent>
                    <Grid spacing={2} container>
                      <Grid xs={12} item>
                        <Typography variant="h4">
                          { pastForm.title }
                        </Typography>
                      </Grid>
                      <Grid xs={12} sm={4} item>
                        <Typography>
                          Type:
                          <b>
                            { ` ${pastForm.type}` }
                          </b>
                        </Typography>
                      </Grid>
                      { pastForm.type !== 'Idea' && (
                        <>
                          <Grid xs={12} sm={4} item>
                            <Typography>
                              Priority:
                              <b>
                                { ` ${pastForm.priority}` }
                              </b>
                            </Typography>
                          </Grid>
                          <Grid xs={12} sm={4} item>
                            <Typography>
                              Deadline:
                              <b>
                                { ` ${pastForm.deadline}` }
                              </b>
                            </Typography>
                          </Grid>
                        </>
                      ) }
                      <Grid xs={12} item>
                        <Typography variant="body1" align="justify">
                          { pastForm.description.split('\n').map((item, index) => (
                            <span key={String(index)}>
                              { item }
                              <br />
                            </span>
                          )) }
                        </Typography>
                      </Grid>
                      { pastForm.images.length > 0 && pastForm.images.map(image => (
                        <Grid xs={4} key={image} item>
                          <div
                            style={{
                              display: 'block',
                              width: '100%',
                              paddingTop: '100%',
                              backgroundImage: `url(${image})`,
                              backgroundRepeat: 'no-repeat',
                              backgroundPosition: 'center',
                              backgroundSize: 'cover',
                            }}
                          />
                        </Grid>
                      )) }
                    </Grid>
                  </CardContent>
                </Card>
              )) }
            </Grid>
            <Grid item />
          </Grid>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  Logic: PropTypes.shape().isRequired,
};

export default withLogic(App);
