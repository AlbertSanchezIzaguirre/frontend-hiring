import React from 'react';

const LogicContext = React.createContext();

export default LogicContext;
