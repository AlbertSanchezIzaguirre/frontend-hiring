import React from 'react';
import LogicContext from './LogicContext';

export default function withLogic(Component) {
  return props => (
    <LogicContext.Consumer>
      { Logic => <Component {...props} Logic={Logic} /> }
    </LogicContext.Consumer>
  );
}
