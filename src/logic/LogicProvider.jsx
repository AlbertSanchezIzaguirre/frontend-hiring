import React, { Component } from 'react';
import PropTypes from 'prop-types';

import LogicContext from './LogicContext';

function getCleanForm() {
  return {
    title: '',
    type: 'default',
    priority: '',
    deadline: new Date().toJSON().slice(0, 10).replace(/-/g, '-'),
    description: '',
    images: [],
  };
}

class LogicProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: getCleanForm(),
      pastForms: [{
        date: new Date(),
        title: 'Add Timer',
        type: 'Feature Request',
        priority: '5',
        deadline: new Date().toJSON().slice(0, 10).replace(/-/g, '-'),
        description: 'When a Rap Battle starts, the host must be aware of how much time is left for that specific battle.\nSetting a Timer on the big screen will also help the competitors to know when there time is up.',
        images: [
          './sample1.png',
          './sample2.png',
          './sample3.png'
        ],
      }],
    };
  }

  updateForm(value) {
    this.setState(prevState => ({
      form: {
        ...prevState.form, ...value,
      },
    }));
  }

  submitForm(form) {
    this.setState(prevState => ({
      form: getCleanForm(),
      pastForms: [
        { ...form, date: new Date() },
        ...prevState.pastForms
      ],
    }));
  }

  resetForm() {
    this.setState({ form: getCleanForm() });
  }

  render() {
    const { form, pastForms } = this.state;
    return (
      <LogicContext.Provider value={{
        form: form,
        pastForms: pastForms,
        updateForm: value => this.updateForm(value),
        submitForm: value => this.submitForm(value),
        resetForm: () => this.resetForm(),
      }}
      >
        { this.props.children }
      </LogicContext.Provider>
    );
  }
}

LogicProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default LogicProvider;
