import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { blue, lightBlue } from '@material-ui/core/colors';
import PropTypes from 'prop-types';

import './Theme.css';

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: lightBlue,
  },
});

function Theme(props) {
  return (
    <ThemeProvider theme={theme}>
      { props.children }
    </ThemeProvider>
  );
}

Theme.propTypes = {
  children: PropTypes.shape().isRequired,
};

export default Theme;
